local _M = {}

function _M.do_module_process(name, default_name, module_prefix, ...)
	local final_name = (name ~= nil) and (name ~= "") and name or default_name
	local module_name = (string.gmatch(final_name, "%w+")() or default_name):lower()
	-- try to use require to load the module (with the ability to catch an error)
	local status, module = pcall(require, module_prefix .. module_name)
	-- if not successful use require to load default module (without catching an error)
	if not status then
		module_name = default_name:lower()
		module = require(module_prefix .. module_name)
	end
	return module.process(module_name, ...)
end

return _M
